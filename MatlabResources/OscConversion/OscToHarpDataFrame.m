function [HarpDataFrame] = OscToHarpDataFrame(oscMessage)
%OSCTOHARPDATAFRAME Converts simple Osc packet into a Harp Data Frame

% Remove Osc header
% Must use Osc address equal to "/   " and TypeTage equal to "b"
HarpDataFrame = typecast(oscMessage(13:end-1), 'uint8');

end