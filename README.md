## Introduction

The Harp technology was originaly thought to suppress the lack of efficient binary protocol for hardware comunication.

On this repository you can find:

* Documentation of the Harp Binary Protocol, what is a Harp Device and the Harp Timestamp Bus,
* The CSV Converter, a GUI that is used to convert from Harp binary files to CSV format,
* LabVIEW resources used for the GUIs of each device, and
* C code for microcontrollers.

## List of the current devices' IDs reported

| ID |Device |Owner  |
|-|-|-|
|1024 |Poke |Champalimaud Foundation
|1040 |Multi Pwm Genetayor |Champalimaud Foundation
|1056 |Wear |Champalimaud Foundation
|1072 |12 Volts Drive |Champalimaud Foundation
|1088 |Led Controller |Champalimaud Foundation
|1104 |Synchronizer |Champalimaud Foundation
|1121 |Simple Analog Generator |Champalimaud Foundation
|1136 |Archimedes |Champalimaud Foundation
|1152 |Clock Synchronizer |Champalimaud Foundation
|1168 |Camera Controller |Champalimaud Foundation
|1184 |PyControl Adapter |Champalimaud Foundation
|1216 |Behavior |Champalimaud Foundation
|1232 |Load Cells Reader |Champalimaud Foundation
|1248 |Audio Switch |Champalimaud Foundation
|1264 |Rgbs |Champalimaud Foundation
|1200 |FlyPad |
|2048-2063| Reserved | MindReach
|2064-2079| Reserved | Neurophotometrics

## Licensing

The code and documentation available in this repository is free software: you can redistribute it and/or modify it under the terms of the "The MIT License".
Check the file LICENSE available in this directory for more information.